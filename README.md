# Django Url Shortener  #

Используя сервис, пользователь может получить короткий URL к веб-странице

![Github API userdata](https://imageshack.com/a/img922/8606/tORQps.png)

### URL установленного проекта ###
http://81.2.248.93:8002

### Ключевые технологии ###

* [Django 1.11.6] (https://www.djangoproject.com)
* [Django-Widget-Tweaks 1.4.1] (https://pypi.python.org/pypi/django-widget-tweaks)

### Кратко проекте ###

* Работа с формой (получение параметры из формы)
* Работа с базой данных Sqlite3
* Использование Class based view
* Передача контекста в html шаблон
* Получение и вывод контекста клиенту (в браузер)
* Deploy проекта на VPS Ubuntu, Nginx, Wsgi

### Контакты ###

* rvk.sft[ at ]gmail.com