from django.shortcuts import render, get_object_or_404, reverse
from django.views.generic import CreateView, DetailView, RedirectView
from .models import UrlRecord
from .forms import HomeForm
from short_urls.settings import FULL_LINK_URL, SHORT_LINK_URL


class HomeView(CreateView):
    template_name = 'index.html'
    model = UrlRecord
    form_class = HomeForm

    def get_success_url(self, **kwargs):
        return self.object.full_link

class DetailLinkView(DetailView):
    model = UrlRecord
    template_name = 'urlrecord_detail.html'

    def get_object(self, **kwargs):
        return get_object_or_404(UrlRecord, short_link=SHORT_LINK_URL+self.kwargs.get("short_link"))


class RedirectUrlView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        obj = get_object_or_404(UrlRecord, short_link=SHORT_LINK_URL+kwargs['full_link'])
        return obj.link