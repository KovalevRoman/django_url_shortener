from django.db import models
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from .utils import unique_short_link_generator, get_hostname
import os
from short_urls.settings import SHORT_LINK_URL, FULL_LINK_URL

class UrlRecord(models.Model):
    # name            = models.CharField(max_length=500, blank=False, null=False)
    link            = models.URLField(max_length=100, blank=True)
    short_link      = models.CharField(max_length=10, default=None)
    full_link       = models.CharField(max_length=100, default=" ")

    def __str__(self):
        return self.link

    def get_hostname(self, request):
        return request.get_host()

def url_presave_receier(sender, instance, *args, **kwargs):
    if not instance.short_link or not instance.full_link:
        instance.short_link=unique_short_link_generator(instance)
        instance.full_link = ''
        instance.full_link=FULL_LINK_URL+instance.short_link
        instance.short_link = SHORT_LINK_URL + instance.short_link
        instance.save()


pre_save.connect(url_presave_receier, sender=UrlRecord)
