import random
import string
import socket


def random_string_generator(size=5, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def unique_short_link_generator(instance, new_short_link=None):
    if new_short_link is not None:
        short_link = new_short_link
    else:
        short_link=random_string_generator()

    Klass = instance.__class__
    qs_exists = Klass.objects.filter(short_link=short_link).exists()
    if qs_exists:
        new_short_link = random_string_generator()
        return unique_short_link_generator(instance, new_short_link=new_short_link)
    return short_link

def get_hostname():
    hostname = socket.gethostname()
    return hostname