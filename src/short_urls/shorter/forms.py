from django import forms
from django.forms import ModelForm, Textarea
from .models import UrlRecord

class HomeForm(ModelForm):

    class Meta:
        model = UrlRecord
        fields = ('link',)
        labels = {'link':''}